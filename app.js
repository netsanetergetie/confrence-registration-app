const express= require('express');
const path = require('path');
const podyParser= require('body-parser');
const cors = require('cors');
const passport= require('passport');
const mongoose =require('mongoose');
var session = require('express-session');

const config = require('./config/database');
mongoose.connect(config.database);
// on connection 
mongoose.connection.on('connected',()=>
{
    console.log(' connected to databse'+ config.database)
})

// error 
mongoose.connection.on('error',()=>
{
    console.log('databse error'+ err)
})



const app = express();


const users= require('./routes/users');
const bodyParser = require('body-parser');
//port number
const port = 3000;
//set stastic folder 
app.use(express.static(path.join(__dirname,'public')))

//cors middle ware 
app.use(cors());

// body parser middleware 
app.use(bodyParser.json())

// passport middleware
app.use(passport.initialize())
//app.use(passport.session())
// set session for passport oauth login
var session = require('express-session');
app.use(session({ secret: 'SECRET' }));
//let start using passport for authontication
require('./config/passport')(passport);

app.use('/users', users)




//index route
app.get('/',(req,res)=>{
    res.send(' invalid endpoint')
})
//start server 

app.listen(port,()=>{
console.log('server started on port: ' + port);
}) ;
