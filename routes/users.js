const express = require('express')
const router = express.Router();
const passport = require('passport')
const jwt = require('jsonwebtoken')
const User = require('../models/users');
const config = require('../config/database')

// router.post('/testme',(req, res, next)=>{
//     res.send("testme eski POST");
// });
// register router so that instead of using app.get or app.post() we can use router.get/router.post()
// so that we can use /user/register ... since we are in users file we don't need to put /users
router.post('/register',(req, res, next)=>{
//res.send('REGISTER');
let newUser = new User({
   name: req.body.name,
   email: req.body.email,
   username: req.body.username,
   password:  req.body.password
});
  User.addUser(newUser,(err,user)=>{
    if(err){
        res.json({success:false,msg:'failed to register user'})
    }else{
    res.json({ success: true, msg : 'User registerd'})
  }});
});
// Autenticate
router.post('/authenticate',(req,res,next)=>{
    const username =req.body.username;
    const password = req.body.password;

    User.getUserByUsername(username, (err, user) =>{
        if(err) throw err;
        if(!user){
            return res.json({success: false, msg: "User not found"});
        }
        User.comparePassword(password, user.password, (err, isMatch) => {
            if(err) throw err;
            if(isMatch){
                //const token = jwt.sign(user,config.secret, {
                // const token = jwt.sign({ data: user }, config.secret, {
                //     expiresIn: 604800 // 1 week
                // });
                const token = jwt.sign(user.toJSON(), config.secret, {
                    expiresIn: 604800
                   }); 
                res.json({
                    success: true,
                    token: 'JWR '+token,
                    user:{
                        id: user._id,
                        name: user.name,
                        email: user.email
                    }
                })
            }else{
                return res.json({success: false, msg: "Wrong password"})
            }
        })
    });
});
// profile
router.get('/profile', passport.authenticate('jwt', {session: false}), (req,res,next)=>{
    //res.send('PROFILE');
    res.json({user: req.user});
});
// validate
router.get('/validate',(req,res,next)=>{res.send('VALIDATE');
});

module.exports = router;
