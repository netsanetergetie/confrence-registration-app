# Description

This is part of project work for the course "Software Development Skills: Full-Stack 2021-22""
The project contains implmentation of confrence registration and login app using MEAN stack
- The REST API is built using the following technologies
 - > NODE.js,  Express, MangoDB, and Passport
 - > Create some end point for registration
 - > Authentications using passport and  Jesson Web Tokens (JWT) to generate web token 

 # Notes:
 <ul>
  <li> Make sure CORS: Cross-Origin Resource sharing is enabled;
     This is because as the backend server and our front end Angular app will be on different ports, so inoder 
     to make a request from the front end we need to enable CORS
 <li> Backend: Mongoose are used for database conncetivy and storage.

<li> Angular Router and HTTP Module 
<li> Angular2-JWT: For token authentication
 </ul>

 # usage
 The app run pn localhost with port 4200.  
 The home page contains some description about the confrence.
 It has:
  - Registration page: 
   -- Where users can register and submit.
   --  The registration form has format checking for email address nad empty forms
   -- succecfuly registered user will be registerd to the mongodb database
 - Login page:
   -- Registed user can login to the dashboard using username and passpwrd
   -- The loging page will take user input and authenticate

